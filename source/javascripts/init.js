var js = js || {},
  body = $('body'),
  doc = $(document);

js.main = {
  init: function () {
    this.linksExternal();
    this.menuTrigger();
    this.waypoints_logo();
    this.waypoints_masthead();
  },
  linksExternal: function () {
    $("a[href^='https://']").attr("target", "_blank");
    $("a[href^='http://']").attr("target", "_blank");
  },
  menuTrigger: function () {
    var nav = $(".site-nav");
    var body = $("body");
    $("#menu-trigger").on("click", function(){
      nav.addClass("active");
      body.addClass("overflow");
    });
    $(".close-button").on("click", function(){
      nav.removeClass("active");
      body.removeClass("overflow");
    });
    $(".site-nav-link").on("click", function(){
      nav.removeClass("active");
      body.removeClass("overflow");
    });

    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  },
  waypoints_logo: function () {
    var waypoints = $('#booknow').waypoint({
      handler: function() {
        $(".site-masthead-logo").toggleClass("white");
      },
    });
  },
  waypoints_masthead: function () {
    var waypoints = $('#individuals').waypoint({
      handler: function() {
        $(".site-masthead").toggleClass("fold");
      },
    });
  }
};

doc.ready(function () {
  js.main.init();
});